# README

This is the backend code for customer Support

1. Requirements
    * ruby 2.3.0
    * mysql 5.7.16
    


Steps to setup

* clone the code
    
* run bundle install

    cd customer_support
    
    bundle install
*   db migrations

    rails db:create db:migrate db:seed
    
* start server

    rails s